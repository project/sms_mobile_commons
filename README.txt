The master branch of this module is intentionally empty. Please clone a proper
version branch like '7.x-1.x'. For instructions see the project's
Git tab at https://drupal.org/project/sms_mobile_commons/git-instructions.
